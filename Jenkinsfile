pipeline {
    agent {
        node {
            label "master"
        }
    }

    parameters {
        booleanParam(name: 'Destroy', defaultValue: false, description: 'Tick to destroy')
        string(name: 'Version', defaultValue: 'master', description: 'Version: v1.0, v1.1')
        choice(name: 'Location', choices: [ "East US", "East US 2", "South Central US", "West US 2", "West US 3", "Australia East", "Southeast Asia", "North Europe", "UK South", "West Europe", "Central US", "North Central US", "West US", "South Africa North", "Central India", "East Asia", "Japan East", "Jio India West", "Korea Central", "Canada Central", "France Central", "Germany West Central", "Norway East", "Switzerland North", "Brazil South", "Australia", "Brazil", "Canada", "Europe", "India", "Japan", "West Central US", "South Africa West", "Australia Central", "Australia Central 2", "Australia Southeast", "Japan West", "Korea South", "South India", "West India", "Canada East", "France South", "Germany North", "Norway West", "Switzerland West", "Brazil Southeast" ], description: '') 
    }

    environment {
        BUILD_ID = "${env.BUILD_ID}";
        NEXUS_URL = "34.72.187.15:8081";
        BRANCH = "${params.Version}";
        LOCATION = "${params.Location}";
    }
    stages {
        stage('Destroy') {
            when { expression{ params.Destroy.toBoolean() } }
            steps {
                sh 'kubectl delete namespace ingress-basic'
                sh '''
                    cd newrelic
                    terraform destroy -auto-approve
                '''
                sh '''
                    cd aks
                    terraform destroy -auto-approve
                '''
            }
        }
        stage('Checkout') {
            when { expression{ !params.Destroy.toBoolean() } }
            steps {
                checkout([$class: 'GitSCM', branches: [[name: "*/${BRANCH}"]],
                            extensions: [], userRemoteConfigs: [[credentialsId: 'gitlab',
                            url: 'https://gitlab.com/chivy1204/terraform-k8s.git']]])
            }
        }
        stage('Create k8s') {
            when { expression{ !params.Destroy.toBoolean() } }
            steps {
                sh "cd aks && terraform init && terraform apply -auto-approve -var location=\"${params.Location}\""
            }
        }
        stage('Config HTTPS') {
            when { expression{ !params.Destroy.toBoolean() } }
            steps {
                sh '''
                    cd aks
                    az aks get-credentials --resource-group $(terraform output -raw resource-name) --name $(terraform output -raw cluster-name) --overwrite-existing

                    kubectl create namespace ingress-basic
                    
                    helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
                    
                    helm install nginx-ingress ingress-nginx/ingress-nginx --namespace ingress-basic --set controller.replicaCount=2 --set controller.nodeSelector."beta\\.kubernetes\\.io/os"=linux --set defaultBackend.nodeSelector."beta\\.kubernetes\\.io/os"=linux --set controller.admissionWebhooks.patch.nodeSelector."beta\\.kubernetes\\.io/os"=linux --set controller.service.loadBalancerIP="$(terraform output -raw static_ip)" --set controller.service.annotations."service\\.beta\\.kubernetes\\.io/azure-dns-label-name"="$(terraform output -raw dns_label)"
                    
                    kubectl label namespace ingress-basic cert-manager.io/disable-validation=true
                    
                    helm repo add newrelic https://helm-charts.newrelic.com && \
                    kubectl create namespace newrelic ; helm install newrelic-bundle newrelic/nri-bundle \
                     --set global.licenseKey=9e670b4ede061b132869157254ea0848c6e2NRAL \
                     --set global.cluster='ABC' \
                     --namespace=newrelic \
                     --set newrelic-infrastructure.privileged=true \
                     --set ksm.enabled=true \
                     --set prometheus.enabled=true \
                     --set kubeEvents.enabled=true \
                     --set logging.enabled=true 

                    helm repo add jetstack https://charts.jetstack.io
                    
                    helm repo update
                    
                    helm install cert-manager --namespace ingress-basic --version v1.3.1 --set installCRDs=true --set nodeSelector."beta\\.kubernetes\\.io/os"=linux jetstack/cert-manager
                    
                    echo $(terraform output -raw full_dns) > /tmp/webapi
                '''
            }
        }
        stage('Create alert newrelic') {
            when { expression{ !params.Destroy.toBoolean() } }
            steps {
                sh "cd newrelic && terraform init && terraform apply -auto-approve"
            }
        }
    }
}
