# Configure terraform
terraform {
  required_providers {
    newrelic = {
      source  = "newrelic/newrelic"
      version = "~> 2.21.0"
    }
  }
}

# Configure the New Relic provider
provider "newrelic" {
  account_id = "3204522"
  api_key = "NRAK-670LWSHO5SOH9W7RUW4AHBRQL9N"  # usually prefixed with 'NRAK'
  region = "US"                    # Valid regions are US and EU
}

resource "newrelic_alert_policy" "alert" {
  name = "aks policy"
}
resource "newrelic_nrql_alert_condition" "foo" {
  policy_id                    = newrelic_alert_policy.alert.id
  type                         = "static"
  name                         = "foo"
  description                  = "Alert when transactions are taking too long"
  runbook_url                  = "https://www.example.com"
  enabled                      = true
  value_function               = "single_value"
  violation_time_limit_seconds = 3600

  nrql {
    query             = "SELECT average(duration) FROM Transaction where appName = 'ABC'"
    evaluation_offset = 3
  }

  critical {
    operator              = "above"
    threshold             = 5.5
    threshold_duration    = 300
    threshold_occurrences = "ALL"
  }
}
resource "newrelic_alert_channel" "slack" {
  name = "slack"
  type = "slack"

  config {
    channel = "general"
    url     = "https://hooks.slack.com/services/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"
  }
}
resource "newrelic_alert_policy_channel" "alert_policy_slack" {
  policy_id  = newrelic_alert_policy.alert.id
  channel_ids = [
    newrelic_alert_channel.slack.id
  ]
}